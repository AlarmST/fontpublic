const path = require('path')

const cssHelper = require(path.resolve(__dirname) + '/modules/cssHelper.js')

const postcssPresetEnv = require('postcss-preset-env')

const cachedObject = {}

export default {
    target: 'static',
    //mode: 'spa',
    /*
     ** Headers of the page
     */
    head: {
        title: process.env.npm_package_name || '',
        htmlAttrs: {
            lang: 'ru'
        },
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || ''
            }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel:"preload",
                as:"style",
                href: 'https://fonts.googleapis.com/css?family=Oswald|PT+Sans&display=swap'
            },
            {
                rel:"stylesheet",
                media:"print",
                href: 'https://fonts.googleapis.com/css?family=Oswald|PT+Sans&display=swap',
                onload: "this.media='all'"
            }
        ],
        script: [
            { src: '//code.jivosite.com/widget/6F00LptEp1', async:true }
        ]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: '~/components/Loading.vue',
/*    loading: {
        height:'2px',
        color:'#ff4081',
        continuous:true,
        throttle: 0

    },
    //loadingIndicator:'chasing-dots',
    loadingIndicator:{
        name: 'chasing-dots',
        color: '#2196f3',


        background: '#303030'
    },*/
    /*
     ** Global CSS
     */
    css: [
        '@/assets/scss/variables/jsRootVar.scss',
    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        // '~/plugins/resp.client.js',
        '~/plugins/main.js',
        '~/plugins/nuxt-swiper-plugin.client.js'
    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [
        "nuxt-magpie",

        // With options
        [
            "nuxt-magpie",
            {
                path: '/_images', // dir inside /dist where downloaded images will be saved
                extensions: ['jpg', 'jpeg', 'gif', 'png', 'webp'],
                baseUrl: process.env.apiGraphQl, // only download images from a certain url (e.g. your backend url)
                verbose: false, // show additional log info
                concurrency: 10, // max concurrent image downloads
                keepFolderStructure: true, // re-creates original image paths when saving local copies
                replaceInChunks: true, // attempts to replace image urls in the generated javascript chunks
                alias: null // see below for details
            }
        ],
        '@nuxtjs/pwa',
        'nuxt-lazysizes',
        //'@nuxt/image',
        '@/modules/generator',
        '@nuxtjs/google-analytics',
        // Doc: https://github.com/nuxt-community/eslint-module
        '@nuxtjs/apollo',
        'vue-yandex-maps/nuxt',
        '@nuxtjs/router',
        '@aceforth/nuxt-optimized-images',
    
        // '@nuxtjs/eslint-module'
        // Doc: https://github.com/nuxt-community/stylelint-module
        //'@nuxtjs/dotenv',
        [
            'nuxt-validate',
            {
                lang: 'ru'

                // regular vee-validate options
            }
        ],
        [
            'svg-to-vue-component/nuxt',
            {
                // ...
            }
        ]
    ],
    optimizedImages: {
        optimizeImages: true,
        optimizeImagesInDev:true,
    },
    image: {
        screens: {
            xs: 320,
            sm: 640,
            md: 768,
            lg: 1024,
            xl: 1280,
            xxl: 1536,
            '2xl': 1536
        },
        domains: [process.env.apiGraphQl]
    },
    /*
     ** Nuxt.js modules
     */
    modules: [
        '~/generate-after',
        '@nuxtjs/axios',
        '@nuxtjs/style-resources',
        'portal-vue/nuxt',
  /*      [
            'nuxt-image-extractor',
            {
                // (Required) CMS url
                baseUrl: process.env.apiGraphQl,

                // (Optional) Dir where downloaded images will be stored
                path: '/_images',

                // (Optional) Array containing image formats
                extensions: ['jpg', 'jpeg', 'gif', 'png', 'webp', 'svg'],
            }
        ],*/
        '@nuxtjs/sitemap',
        [
            '@nuxtjs/yandex-metrika',
            {
                id: '74822515',
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            }
        ],
    ],
    /*
     ** Build configuration
     */
    /*  svgLoader: {
        svgoConfig: {
            plugins: [
                { removeDimensions: true }, { removeViewBox: false }
            ]
        }
    }, */
    build: {
        babel:{
            plugins: [
                "@babel/plugin-proposal-optional-chaining"
            ],
           
        },
        /*
         ** You can extend webpack config here
         */
        extractCSS: false,
        /*optimization: {
            splitChunks: {
                cacheGroups: {
                    styles: {
                        name: 'styles',
                        test: /\.(css|vue)$/,
                        chunks: 'all',
                        enforce: true
                    }
                }
            }
        },*/
        extend(config, ctx) {
            /*      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'));

            svgRule.test = /\.(png|jpe?g|gif|webp)$/;

            config.module.rules.push({
                test: /\.svg$/,
                use: [
                    'babel-loader',
                    'vue-svg-loader',
                ],

            }); */
        },
        postcss: {
            syntax: 'postcss-scss',
            plugins: {
                'postcss-css-variables': {
                    preserve: true,
                    //variables: cssHelper.jsvarlist()
                },
                'postcss-scrollbar': {},
                'postcss-svg': {
                    dirs: ['icons']
                },
                'postcss-mixins': cssHelper.postcssMixins,
                'postcss-functions': cssHelper.postcssFunctions,

                'postcss-nested': {},
                // 'postcssExtractValue':{}
                //'postcss-custom-properties':false
            },
            order: 'presetEnvAndCssnanoLast',
            preset: {
                stage: 1,
                /*  autoprefixer: {
                    grid: 'autoplace'
                }, */
                features: {
                    'nesting-rules': false,
                    'custom-properties': false
                }
            }
        }
    },
    pwa: {
        meta: {
            name:'Alarm Studio',
            description: "Установка автосигнализаций и защита от угона автомобилей в Москве",
            lang:'ru',
            ogHost:'ru',
            theme_color:'#3236f2',
            mobileAppIOS:true
        }
        
    },
    lazySizes : {
        extendAssetUrls : {
            img : 'data-src',
        },
    },
    /*  browserslist: [
        "> 0.1%",

    ], */
    /*    browserslist: [
        "> 0.5%",
        'not IE 11'
    ], */
    generate: {
        fallback: true,
        dir: 'public',
        //routes: dynamicRoutes,
        routes: ['/','/404','/pandora_rhm03_bt','/pandect_btr101','/pandora_nav035_bt','/pandora_rhm03_t','/pandora_hm06','/pandora_nav035','/pandect_rr102','/pandora_ps330','/pandect_x3110','/pandora_dx_90l','/pandora_dx_50b','/pandora_ux_5300','/pandora_dxl_5000_s','/pandora_dxl_3910_pro','/pandora_dxl_4950','/pandect_x3150','/pandora_dxl_3945_pro','/pandora_dxl_3970_pro','/pandora_dx_90bt','/pandora_dx_90b','/pandora_dx_90b','/pandora_dx_40s','/pandora_dx_50s','/pandora_dxl_4970','/pandora_dxl_4790','/pandora_dx_4g','/pandora_dx_6x','/pandect_x1900_bt','/pandect_x_1900bt3g','/pandora_dxl_4910l','/pandora_x_4g','/pandect_x_3110plus','/pandora_ux_4g','/pandora_ux_4750'],
        },
    render: {
        static: {
            maxAge: 1000 * 60 * 60 * 24 * 7
        }
    },
    sitemap: {
        hostname: 'https://alarmst.ru/',
        trailingSlash: true,
        //routes: ['/','/ustanovka_sighnalizatsii/']
    },
    router: {
        trailingSlash: false,
        base: process.env.PUBLIC_PATH || '/'
    },
    styleResources: {
        // your settings here
        scss: ['@/assets/scss/settings.scss'] // alternative: scss
    },
    env:{
      /*  apiServer:'http://api-privetsosed.sad-systems.ru',
        apiGraphQl:'http://api-privetsosed.sad-systems.ru/graph'*/
/*        apiServer:'https://api.alarm.space.sad-systems.ru',
        apiGraphQl:'https://api.alarm.space.sad-systems.ru/graph'*/
     /*   apiServer:'http://alarm-dev-ru.1gb.ru',
        apiGraphQl:'http://alarm-dev-ru.1gb.ru/graph'*/
        apiServer:'https://api.alarmst.ru',
        apiGraphQl:'https://api.alarmst.ru/graph',
        PUBLIC_PATH:'/',
    },
    publicRuntimeConfig: {
        apiServer:process.env.apiServer,
        apiGraphQl:process.env.apiGraphQl,
        PUBLIC_PATH:process.env.PUBLIC_PATH
    },
    apollo: {
        tokenName: 'apollo-token', // optional, default: apollo-token
        tokenExpires: 10, // optional, default: 7 (days)
        includeNodeModules: true, // optional, default: false (this includes graphql-tag for node_modules folder)
        authenticationType: '', // optional, default: 'Bearer'
        // (Optional) Default 'apollo' definition
        defaultOptions: {
            // See 'apollo' definition
            // For example: default query options
            $query: {
                loadingKey: 'loading',
                fetchPolicy: 'no-cache',
            },

        },
        watchLoading:'~/plugins/apollo-watch-loading-handler.js',
        // optional
        errorHandler: '~/plugins/apollo-error-handler.js',
        // required
        clientConfigs: {
            default: '~/plugins/apollo-config-default.js',
            //auth: '~/plugins/apollo-config-auth.js',

        }
    },
    googleAnalytics: {
        id: 'UA-69239034-1'
    }
   /* vue: {
        config: {
            productionTip: true,
            devtools: true
        }
    }*/
   
}
