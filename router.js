import Vue from 'vue'
import Router from 'vue-router'

import PageIndex from '~/pages/PageIndex.vue'
import PageDefault from '~/pages/PageDefault.vue'
import erorr404 from '~/pages/404.vue'

//import scrollBehavior from '~/router.scrollBehavior'
// const Index =()=>import('~/pages/PageIndex.vue')
 import PageCatalog from '~/pages/PageCatalog.vue'
//const PageCatalog = () => interopDefault(import('~/pages/PageCatalog.vue'))
const PageCatalogPrice = () => interopDefault(import('~/pages/PageCatalogPrice.vue'))
const PageCatalogDetail = () =>
    interopDefault(import('~/pages/PageCatalogDetail.vue'))
const PageContacts = () => interopDefault(import('~/pages/PageContacts.vue'))

function interopDefault(promise) {
    return promise.then((m) => m.default || m)
}

Vue.use(Router)


console.log(process.env.PUBLIC_PATH);

export function createRouter() {
    return new Router({
        mode: process.env.routerMode || 'history',
        base: process.env.PUBLIC_PATH || '/',
        routes: [
            {
                path: '/404',
                component: erorr404
            },
            {
                path: '/',
                component: PageDefault,
                children: [
                    {   
                        name:'index',
                        path: '',
                        component: PageIndex
                    },
                   
                
                    {
                        name:'catalog',
                        path: '/:category?',
                        component: PageCatalog
                    },
                    {
                        name:'catalogDetail',
                        path: '/:category',
                        component: PageCatalogDetail
                    },
                  /*  {
                        name:'catalog-price',
                        path: '/catalogprice',
                        component: PageCatalogPrice
                    },*/

                   
                  /*  {
                        name:'text',
                        path: '/:name?',
                        component: PageContacts
                    }  */,
                  
                ]
            },
           
       
        ],
        //scrollBehavior
    })
}
