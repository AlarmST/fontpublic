import gql from 'graphql-tag'

import lodash_isString from 'lodash/isString'

export default {
    apollo: {

        apiCatalogDetail: {

            query: gql`query($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                tickets {
                    list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                        id
                        title
                        description
                        images,
                        data
                        categories
                        specifications
                        named_id
                        price
                        price_sale
                    }
                    
                }
            }`,
            variables() {
               
                return {
                    filter:{
                        named_id:this.$route.params?.category,

                    }
                }


            },
            update: (data) => { //Missing apiCatalogDetail attribute on result
                return data
            },

            //fetchPolicy: 'cache-first',
            result({data, loading, networkStatus}) {

                
                

                let itemList = data?.tickets?.list;
                if(Array.isArray(itemList)) {
                    if(itemList.length) {
                        let item = itemList[0]
                        if(item && item.data) {

                            if(lodash_isString(item.data)) {

                                item.data = JSON.parse(item.data)
                            }
                            if(lodash_isString(item.specifications)) {item.specifications = JSON.parse(item.specifications)}


                            this.$store.commit('control/setPageData', ['catalogDetail', item])
                            this.$store.commit('control/setPageMeta', item)




                            let catalog_find = this.$store.state.stateCategories.find(find => find.id === item.categories[0])

                            this.$store.commit('control/breadCrumbsPush', {
                                title:catalog_find?.title,
                                to:{
                                    name:'catalog',
                                    params:{
                                        category:catalog_find.name.split('catalog_')[1]
                                    }

                                }
                            })

                            this.$store.commit('control/breadCrumbsPush', {
                                title:item.title,
                                to:{
                                    name:'catalogDetail',
                                    params:{
                                        category:item.named_id
                                    }

                                }
                            })

                        }
                    } else {
                        //this.$router.push('/404')
                        if(process.static) location.href = location.origin + '/404'
                        //else this.$nuxt.error({ statusCode: 404, message: 'not found' })
                    }
                }
            },
            // Error handling

        },
        apiCatalogDetailListCalc: {

            query: gql`query($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                tickets {
                    list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                        id
                        title
                        named_id
                        images,
                        price
                        price_sale
                    
                    }
                    
                }
            }`,

            skip () {

                return !this.apiCatalogDetail?.tickets?.list[0]?.data?.cases?.split(' ').map(item=>Number(item))
            },
            variables() {

            
                let ids = this.apiCatalogDetail?.tickets?.list[0]?.data?.cases?.split(' ').map(item=>Number(item))
              
                return {
                    filter:{
                        ids:ids,

                    }
                }


            },
            update: (data) => { //Missing apiCatalogDetail attribute on result
                return data
            },

            //fetchPolicy: 'cache-first',
            result({data, loading, networkStatus}) {


                let list = data?.tickets?.list
                if(list?.length) {
                    list = list.map(item=>{
                        if(lodash_isString(item.data)) {
                           
                            item.data = JSON.parse(item.data)
                        }
                        return item
                    })
                    /* item.data = JSON.parse(item.data)
                     console.log(item.data);*/
                    this.$store.commit('control/setPageData', ['catalogDetailListCalc', list])
                }

            
             
            },
            // Error handling

        },
        apiCatalogDetailListMore: {

            query: gql`query($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                tickets {
                    list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                        id
                        title
                        named_id
                        images,
                        price
                        price_sale

                    }

                }
            }`,

            skip () {

                return !this.apiCatalogDetail?.tickets?.list[0]?.data?.moreItems?.split(' ').map(item=>Number(item))
            },
            variables() {


                let ids = this.apiCatalogDetail?.tickets?.list[0]?.data?.moreItems?.split(' ').map(item=>Number(item))

                return {
                    filter:{
                        ids:ids,

                    }
                }


            },
            update: (data) => { //Missing apiCatalogDetail attribute on result
                return data
            },

            //fetchPolicy: 'cache-first',
            result({data, loading, networkStatus}) {


                let list = data?.tickets?.list
                if(list?.length) {
                    list = list.map(item=>{
                        if(lodash_isString(item.data)) {

                            item.data = JSON.parse(item.data)
                        }
                        return item
                    })
                    /* item.data = JSON.parse(item.data)
                     console.log(item.data);*/
                    this.$store.commit('control/setPageData', ['catalogDetailListMore', list])
                }



            },
            // Error handling

        },
    }
}
