import gql from 'graphql-tag'

export default {
    apollo: {

        apiCatalog: {

            query: gql`query($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                tickets {
                    list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                       
                        data
                        images,
                    }
                    
                }
            }`,


            variables() {
            
                return {
                    filter: {
                        categories: [
                            
                            this.$store.getters.categoriesNameToId(this.$route.params.name)
                        ]
                    }
                }


            },
            update: (data) => { //Missing apiIndexTop attribute on result
                return data
            },

            //fetchPolicy: 'cache-first',
            result({data, loading, networkStatus}) {
                if(!data) return
                let item = data?.tickets?.list[0]
                if(item && item.data) {
                    
                    item.data = JSON.parse(item.data)
                 
                    this.$store.commit('control/setPageData', ['catalog', item])    
                    this.$store.commit('control/setPageMeta', item)    
                }
              
            },
            // Error handling

        }
    }
}
