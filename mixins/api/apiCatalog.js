import gql from 'graphql-tag'

import lodash_isString from 'lodash/isString'
/*import lodash_cloneDeep from 'lodash/cloneDeep'
import {ApiCategories} from '@/api/Api.js'*/
export default {
    watch:{
        '$route.query'(){
            this.$fetch()
        }
    },
    async fetch(){
        //if(this.$store.getters.categoriesNameToId(this.$route.params.category)==null) return await 
        const variables = ()=>{
            let filter= {
                categories: [

                    this.$store.getters.categoriesNameToId(this.$route.params.category)
                ]
            }
            return {
                filter
            }
        }
        const {data} = await this.$apollo.query({

            query: gql`query apiCatalog ($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                tickets {
                    list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                        data
                        images,
                        title
                    }

                }
            }`,
            variables:variables()
        })
        if(!data) return

        this.$store.commit('control/breadCrumbsPush', {
            title:this.$store.getters.categoriesNameFind(this.$route.params.category)?.title,
            to:{
                name:'catalog',
                params:{
                    category:this.$route.params.category
                }

            }
        })

        let item = data?.tickets?.list[0]
        if(item) {
            if(lodash_isString(item.data)) {

                item.data = JSON.parse(item.data)
            }

            this.$store.commit('control/setPageData', ['catalog', item])
            this.$store.commit('control/setPageMeta', item)

            this.$nextTick(() => {
                this?.$nuxt?.$loading?.finish()
            })





        }

    },
    methods:{
      /*  async getApiCatalog({store,app,params}){
            let $apollo = app.apolloProvider.defaultClient

            let obj =  {
                $apollo
            }
            await ApiCategories.list.call(obj,{}).then(data=>{
                console.log('setCategoryList');
                if(data) store.commit('setCategoryList',lodash_cloneDeep(data.list))
                return
            })
            
            console.log('start',store.getters.categoriesNameToId(params.category));
            const variables = ()=>{
                let filter= {
                    categories: [

                        store.getters.categoriesNameToId(params.category)
                    ]
                }
                return {
                    filter
                }
            }
            const {data} = await app.apolloProvider.defaultClient.query({

                query: gql`query apiCatalog ($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                    tickets {
                        list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                            data
                            images,
                            title
                        }

                    }
                }`,
                variables:variables()
            })
            if(!data) return
            
            store.commit('control/breadCrumbsPush', {
                title:store.getters.categoriesNameFind(params.category)?.title,
                to:{
                    name:'catalog',
                    params:{
                        category:params.category
                    }

                }
            })

            let item = data?.tickets?.list[0]
           
            if(item) {
                if(lodash_isString(item.data)) {

                    item.data = JSON.parse(item.data)
                }
                console.log(item);
                store.commit('control/setPageData', ['catalog', item])
                store.commit('control/setPageMeta', item)
                
            }
            return 
        }*/
    }
  /*  apollo: {

        apiCatalog: {

            query: gql`query($limit:Int,$offset:Int,$filter:TicketFilter,$order: [TicketOrderBy]){
                tickets {
                    list (limit:$limit,offset:$offset,filter:$filter,order:$order) {
                        data
                        images,
                        title
                    }
                    
                }
            }`,

            skip () {
                return this.$store.getters.categoriesNameToId(this.$route.params.category)==null
            },
            variables() {

                let categoryName = this.$route.params.category
              let filter= {
                    categories: [

                        this.$store.getters.categoriesNameToId(this.$route.params.category)
                    ]
                }
                return {
                    filter
                }


            },
            update: (data) => { //Missing apiIndexTop attribute on result
                return data
            },


            result({data, loading, networkStatus}) {
                if(!data) return
                
                this.$store.commit('control/breadCrumbsPush', {
                    title:this.$store.getters.categoriesNameFind(this.$route.params.category).title,
                    to:{
                        name:'catalog',
                        params:{
                            category:this.$route.params.category
                        }

                    }
                })
               
                let item = data?.tickets?.list[0]
                if(item) {
                    if(lodash_isString(item.data)) {

                        item.data = JSON.parse(item.data)
                    }
             
                    this.$store.commit('control/setPageData', ['catalog', item])
                    this.$store.commit('control/setPageMeta', item)

                    this.$nextTick(() => {
                        this?.$nuxt?.$loading?.finish()
                    })

                   
                    
                    
                  
                }
                
              
            },
            // Error handling

        }
    },*/
    
}
