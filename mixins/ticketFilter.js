function ticketFilterCategories(routeParams,storeCategories){

    if(routeParams.category) { //Если есть категория значит, она последняя, значит ищем 1 индификатор
        let categoryFind = storeCategories.find(category=>category.name===routeParams.category)
        if(categoryFind) {

            return categoryFind.id
        }

    } else if (routeParams.subsection) { // Нет категории но есть под раздел, значит вытягиваем все дочернии категории
        let subsectionFind = storeCategories.find(category=>category.name===routeParams.subsection)
        if(subsectionFind) {
            let categories = storeCategories.filter(category=>category.parent_id===subsectionFind.id).map(category=>category.id)
            return [subsectionFind.id,...categories]
        }

    } else if (routeParams.section) { // Нет категории и нет под раздела, значит вытягиваем все дочернии категории под разделов

        let sectionFind = storeCategories.find(category=>category.name===routeParams.section)
        if(sectionFind) {

            let subSectionsIds = storeCategories.filter(category=>category.parent_id===sectionFind.id).map(category=>category.id)

            if(subSectionsIds.length) {
                let categories = []
                subSectionsIds.forEach(subSectionId=>{
                    let list = storeCategories.filter(category=>category.parent_id===subSectionId).map(category=>category.id)
                    categories.push(...list)
                })

                return categories = [sectionFind.id,...subSectionsIds,...categories]
            }
        }

    }
    ///else {return storeCategories.map(category=>category.id)}




    return []


}

import QUERY_TICKET_FILTER from '@/api/gql/queryTicketFilter.gql'
import QUERY_TICKET_ORDER from '@/api/gql/queryTicketOrder.gql'
import mutationSetTicketFilter from '@/api/gql/mutationSetTicketFilter.gql'

export default {
    apollo:{
        ticketFilter:{

            query:QUERY_TICKET_FILTER,
            update({ticketFilter}){


                if(!ticketFilter.is_auction) ticketFilter = this.$_clearTypeNameField(ticketFilter,'is_auction') //не отправляем поле в запрос
                if(ticketFilter.author_id===-1) ticketFilter = this.$_clearTypeNameField(ticketFilter,'author_id') //не отправляем поле в запрос
                
                return this.$_clearTypeNameField(ticketFilter)
            },

        },
        ticketOrder:{

            query:QUERY_TICKET_ORDER,
            update({ticketOrder}){
                return  this.$_clearTypeNameField(ticketOrder)
            }
        },
    },
    watch:{
        '$route':{
            handler(to) {
                this.$apollo.mutate({
                    mutation: mutationSetTicketFilter,
                    variables: {
                        payload:{
                            categories:ticketFilterCategories(this.$route.params,this.$store.state.stateCategories)
                        }
                    }
                })
            },
            immediate: true
        }
    },
}
