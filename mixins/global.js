import paramCase from '@/modules/paramCase'

export default {
    computed: {
        componentName() {
            return paramCase(this.$options._componentTag)
        }
    }
}
