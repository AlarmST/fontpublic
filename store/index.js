



import _clonDeep from 'lodash/cloneDeep'

export const state = () => ({

  
    sections:{
        ticket:{
            title:'Объявления'
        }
    },

    stateCategories:[],

})


export const mutations = {
	setCategoryList (state,list) {

        let findIndex = list.findIndex(item=>item.name==="catalog_avtosighnalizatsii_pandora")
        if(findIndex !=-1) {
            list[findIndex].hide=true
        
        }

        findIndex = list.findIndex(item=>item.name==="avtosighnalizatsii_pandora")
        if(findIndex !=-1) {
            list[findIndex].hide=true
        
        }

       

		state.stateCategories = list
	}
}



export const actions = {

}

export const getters = {
    catalog:(state,getters)=> {
        
        let find = getters.categories.find(item=>item.name==='pages')
   
        if(!find) return
        if(!find.itemList) return
        if(!find.itemList.find(item => item.name === 'pagesCatalog')) return

        function compareNumbers(a, b) {
            return a.priority - b.priority;
        }


        return find.itemList.find(item=>item.name==='pagesCatalog').itemList.sort(compareNumbers)
    },
    pagesText:(state,getters)=> {
        let find = getters.categories.find(item=>item.name==='pages')
        return find && find.itemList && find.itemList.find(item=>item.name==='pagesText').itemList
    },
    categories:state=> state.stateCategories.map(itemComp,{state,level:-1}).filter(item=>item.parent_id===0 || item.parent_id==null),
    categoriesRoute: state => category => {
     
        let params = {

        }
        params.section = category.name
        if(category.level===1) {

            let categoryLevel0 = state.stateCategories.find(item=>item.id===category.parent_id)
            params.section = categoryLevel0.name
            params.subsection = category.name
        }
        if(category.level===2) {
            let categoryLevel1 = state.stateCategories.find(item=>item.id===category.parent_id)
            let categoryLevel0 = state.stateCategories.find(item=>item.id===categoryLevel1.parent_id)
            params.section = categoryLevel0.name
            params.subsection = categoryLevel1.name
            params.category = category.name
        }

      
        return {name:'catalog',params}

    },
    categoriesNameToId: (state,getters) => categoryName => {
       // console.log(categoryName);
        let find = state.stateCategories.find(item=>item.name===categoryName)
        if(find) return find.id
        return null
      

    },
    categoriesNameFind: (state,getters) => categoryName => {

        

        return state.stateCategories.find(item=>item.name===categoryName)

    }
}

function itemComp (item,index){
    let category = _clonDeep(item)
    category.level = this.level+1

    let childCategoryList = this.state.stateCategories.filter(SCItem=>SCItem.parent_id===category.id)
    if(childCategoryList.length) {
        //console.log(childCategoryList);
        category.itemList = childCategoryList.map(itemComp,{state:this.state,level:category.level});
        //category.childrenShow=false
    }

    //if(!category.name) category.name=category.title





    return category
}

/*
export const getters = {
    categories:state=> state.stateCategories.map(itemComp,{level:-1})

}

function itemComp (item,index){
    let category = _clonDeep(item)
    category.id=index;

    category.level = this.level+1
    if(!category.name) category.name=category.title

    if(category.itemList) {
        category.itemList = category.itemList?.map(itemComp,{level:category.level});
        category.childrenShow=false
    }

    return category
}
*/


