



import apolloConfig from '@/plugins/apollo-config.js'

export default function(context){
    return apolloConfig(process.env.apiGraphQl)
}


