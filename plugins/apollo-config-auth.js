import config from '@/config/index'



import apolloConfig from '@/plugins/apollo-config.js'

export default function(context){
    return apolloConfig(config.apiQraphqlAuth)
}
