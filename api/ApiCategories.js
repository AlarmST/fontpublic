import QqlCategoriesAdd from '@/api/gql/QqlCategoriesAdd.gql'
import gqlCategoryList from '@/api/gql/CategoryList.gql'

import gql from 'graphql-tag'

import lodash_cloneDeep from 'lodash/cloneDeep'



export default {

        add({name,title,priority,parent_id}) {
            return this.$apollo.mutate({
                //client:'auth',
                mutation:gql`mutation ($title: String!, $name: String!,$priority:Int, $parent_id:Int) {
                    categories{
                        add(title: $title, name: $name,parent_id:$parent_id, priority:$priority)
                    }
                }`,
                variables: {name,title,priority,parent_id}
            }).then(({data:{categories:{add:id}}}) => id);


        },
        edit({id,name,title,priority,parent_id}) {

            return this.$apollo.mutate({
                //client:'auth',
                mutation:gql`mutation ($id: Int!,$title: String!, $name: String!,$priority:Int, $parent_id:Int) {
                    categories{
                        edit(id:$id,title: $title, name: $name, priority:$priority,parent_id:$parent_id)
                    }
                }`,
                variables: {id,name,title,priority,parent_id},
                update: (cache, { data:{categories:{edit}} }) => {

                    if(edit) {
                        try {
                            let {categories} = cache.readQuery({ query: gqlCategoryList })




                            if (categories.list) {
                                let findIndex = categories.list.findIndex(category => category.id === id)
                                if (findIndex!==-1) {
                                    let findCategory = categories.list[findIndex]
                                    findCategory.title = title
                                    findCategory.name = name
                                    findCategory.parent_id = parent_id
                                 /*   this.$set('list',findIndex,findCategory)
                                    categories.list = list*/

                                    categories.list.splice(findIndex, 1, findCategory)

                                    this.$store.commit('setCategoryList',lodash_cloneDeep(categories.list))

                                }

                            }

                           
                            cache.writeQuery({
                                query: gqlCategoryList,
                                data: {
                                    categories
                                }
                            });
                        } catch (e) {
                     
                            // We should always catch here,
                            // as the cache may be empty or the query may fail
                        }
                    }
                }
            }).then(({data:{categories:{edit:id}}}) => id);


        },

        remove(id) {
            return this.$apollo.mutate({
                //client:'auth',
                mutation:gql`mutation ($id:Int!) {
                    categories{
                        remove(id:$id)
                            __typename 
                    }
                }`,
                variables: {id},
                optimisticResponse: {
                    __typename: 'Mutation',
                    categories:{
                        remove:true
                    }
                },
                update:async (cache, { data}) => {

                    if(data.categories.remove) {
                        try {
                            let data = cache.readQuery({ query: gqlCategoryList })
                            let {categories} = data
                            //console.log(categories);
                            //categories = lodash_cloneDeep(categories)



                            if (categories.list) {
                                let newList = categories.list.filter(category => {
                                    if(category.id === id) {
                                        categories.count-=1;
                                        return false
                                    }
                                    return true

                                })


                                    this.$store.commit('setCategoryList',lodash_cloneDeep(newList))

                                categories.list = newList

                            }





                            await cache.writeQuery({
                                query: gqlCategoryList,
                                data: {
                                    categories
                                }
                            });
                        } catch (e) {
                          
                            // We should always catch here,
                            // as the cache may be empty or the query may fail
                        }
                    }

                }
            }).then(({data:{categories:{remove}}}) => remove);


        },


        list({limit, offset,parent_ids,recursive}) {

            let order = [{
                field:'priority',
                direction:'asc'
            }]



            //console.log(this.$apollo.query());
            return this.$apollo.query({
                query:gqlCategoryList,
                variables: {
                    limit,
                    offset,
                    parent_ids,
                    recursive,
                    //order
                },
                //prefetch:true,
                //fetchPolicy: "network-only",
                result ({ data, loading, networkStatus }) {
                  
                },
                // Error handling
                error (error) {
                  
                },
            }).then(({data:{categories}}) => {


                let {list} = categories;



                let listChange = list.map(itemComp,{list,level:-1}).filter(item=>item.parent_id===0 || item.parent_id==null)
                categories.listChange = listChange
                return categories

            }).catch(r=>{
                console.log(r);
            });


        },


}

import _clonDeep from 'lodash/cloneDeep'
function itemComp (item,index){
    let category = _clonDeep(item)
    category.level = this.level+1

    let childCategoryList = this.list.filter(SCItem=>SCItem.parent_id===category.id)
    if(childCategoryList.length) {
        //console.log(childCategoryList);
        category.itemList = childCategoryList.map(itemComp,{list:this.list,level:category.level});
        //category.childrenShow=false
    }

    //if(!category.name) category.name=category.title





    return category
}
