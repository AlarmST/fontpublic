
import QUERY_TICKET_LIST from '@/api/gql/QUERY_TICKET_LIST.gql'
import QUERY_TICKET_ITEM from '@/api/gql/queryTicketItem.gql'
import mutationTicketAdd from '@/api/gql/mutationTicketAdd.gql'
import mutationTicketEdit from '@/api/gql/mutationTicketEdit.gql'
import mutationTicketRemove from '@/api/gql/mutationTicketRemove.gql'






import lodash_cloneDeep from 'lodash/cloneDeep'

import _omit from 'lodash/omit'



function tost(errors,success){

    errors?.forEach(errors=>this.$toast.error(errors.message))

    if(!success) {
        this.$toast.error('Отказано')
    } else {
        this.$toast.success('Успешно')
    }
}

export default {
        add(payload) {



            return this.$apollo.mutate({
                mutation:mutationTicketAdd,
                //client:'auth',
                variables: {payload},
       
            }).then(({data:{tickets:{add}}}) => add);


        },
    edit(payload) {
        //console.log(payload);
        payload = lodash_cloneDeep(payload)
        //payload.title='zzzzz'
        let {id} = payload
        let optimisticPayload = payload



        payload = _omit(payload,['__typename', 'author','id'])

        return this.$apollo.mutate({
                //client:'auth',
                mutation:mutationTicketEdit,
                variables: {
                    id,
                    payload
                },
            optimisticResponse:{
                tickets:{
                    edit:{
                        ...optimisticPayload,

                    },
                    __typename: "TicketMutation",
                },
            },
        
            })
            .then(async ({errors,data:{tickets:{edit}}}) => {
           
            tost.call(this,errors,edit)
            //console.log(edit);
                //await sleep(3000)

                return edit


            }).catch((r)=>{
              

                let {networkError:{result:{errors}}} = r
            tost.call(this,errors)
        });
        },
        remove(id) {
            return this.$apollo.mutate({
                client:'auth',
                mutation:mutationTicketRemove,
                variables: {id},
            }).then(({errors,data:{tickets:{remove}}}) => {
                tost.call(this,errors,remove)
                if(remove) return remove
            });
        },
 


        list({limit=40, offset}) {
            //console.log(this.$apollo.query());
            return this.$apollo.query({
                query:QUERY_TICKET_LIST,
                variables: {limit, offset},
                //prefetch:true,

                result ({ data, loading, networkStatus }) {
                    //console.log('We got some result!',data)
                },
                // Error handling
                error (error) {
                    console.error('We\'ve got an error!', error)
                },
            }).then(({data:{tickets}}) => tickets)


        },
        item(filter) {
            //console.log(this.$apollo.query());
            return this.$apollo.query({
                skip () {
                    return (this.filter===undefined)
                },
                query:QUERY_TICKET_ITEM,
                variables: {filter},


            }).then(({data:{tickets}}) => tickets.list[0])


        },

}
